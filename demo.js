(function() {

  'use strict'

  // 文件Blob对象
  let file = {};

  // 文件名
  let key = 'test';

  // 后端给的token
  let ak = '';
  let sk = '';
  let bucket = '';
  let token = '';

  // 额外参数
  let putExtra = {
    fname: 'test2',       // 文件原文件名
    params: {},           // 用来放置自定义变量
    mimeType: [] || null, // 用来限制上传文件类型
  }

  // 设定
  let config = {
    useCdnDomain: false,            // 使用CDN加速
    disableStatisticsReport: false, // 是否禁用日志
    region: qiniu.region.z0,        // 上传域名区域
    retryCount: 3,                  // 上传自动重试次数
    concurrentRequestLimit: 3,      // 分片上传的并发请求量
    checkByMD5: false,              // 是否开启 MD5 校验
    
  }

  let ob = {

    // 接收上传进度信息
    next: function(res){
      console.log('next', res);
    },

    // 上传错误后触发
    error: function(err){
      console.log('error', err);
    }, 

    // 接收上传完成后的后端返回信息
    complete: function(res){
      console.log('complete', res);
    }
  };

  document.querySelector('#upload').addEventListener('change', function(e) {
    ak = document.querySelector('#ak').value;
    sk = document.querySelector('#sk').value;
    bucket = document.querySelector('#bucket').value;
    token = getToken(ak, sk, {
      scope: bucket, 
      deadline: 3153600000
    });
    if(e.target.files.length <= 0) return;
    file = e.target.files[0];
    // 青牛云上传对象
    let observable = qiniu.upload(file, key, token, putExtra, config);
    let subscription = observable.subscribe(ob) // 上传开始
  });

  
  // or
  // var subscription = observable.subscribe(next, error, complete) // 这样传参形式也可以
  // subscription.unsubscribe() // 上传取消


})();